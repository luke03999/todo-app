

// Chiamata singola in HTTP post
async function addTodo(desc) {
    await fetch('http://localhost:8080/api/todos', {
        method: 'POST',
        headers: {'Content-type' : 'application/json'},
        body: JSON.stringify({description: desc})
    }, );

    // Dopo aver aggiunto
    reloadTodo();
}

// CHE


// questa riceve l'id del todo ed esegue la chimata
async function deleteTodo(id) {
    await fetch('http://localhost:8080/api/todos/' + id, {
        method: 'DELETE',
        headers: {'Content-type' : 'application/json'}
    }, );
    reloadTodo();
}


async function reloadTodo() {
    var todos = await (await fetch('http://localhost:8080/api/todos')).json();
    var ulList = document.getElementById('todosList');

    ulList.innerHTML = "";

    for (var i = 0; i < todos.length; i++) {
        // Creo il tag li
        var liElement = document.createElement('li');
        var spanElement = document.createElement('span');
        var removeButton = document.createElement('button');
        removeButton.innerText = "X";
        // Creo una nuova funzione con il parametro id bindato in modo da richiamare la
        // funzione deleteTodo con quell'id fisso
        var todo = todos[i];
        var removeThisTodo=deleteTodo.bind(null, todo.id);
        removeButton.addEventListener('click',removeThisTodo);
        // Creo una variabile con l'elemento i-esimo della lista (il singolo todo)

        // mettiamo la descrizione de conetunto come li
        spanElement.textContent = "#" + todo.id + " ";
        spanElement.textContent += todo.description;
        spanElement.textContent += '(';
        if (todo.done) {
            spanElement.textContent += 'Completato';
        } else {
            spanElement.textContent += 'Da completare';
        }

        spanElement.textContent += ')'
        liElement.appendChild(removeButton);
        liElement.appendChild(spanElement);
        // Appendiamo alla lista
        ulList.appendChild(liElement);
    }
}




// Da qui parte la nostra app JS.
// async dice al browser che all'interno di questa funzione
// viene utilizzata una funzione asincrona(await)
// Asincrono: prima esegue tutta la pagina, poi fa la richiesta.
// Sincrono, quando arriva alla richiesta, prima di andare avanti, esegue la richiesta
async function main() {
    console.log('running main');
    // Prendo il tag div con id app
    var divApp=document.getElementById('app');
    // Creao un tag titolo
    var title = document.createElement('h1');
    title.textContent="TODO APP";
    // Aggiungo il tag all'interno
    divApp.appendChild(title);
    // await permette di rendere sincrona un'operazione asincrona
    // Anche json è un operazione asincrona
    var todos = await (await fetch('http://localhost:8080/api/todos')).json();


    console.log(todos);

    // Input text
    var textInput = document.createElement('input');
    textInput.placeholder = 'Inserisci un nuovo todo';
    textInput.id = 'addTodoInput'
    divApp.appendChild(textInput);
    // Button aggiungi
    var addButton = document.createElement('button');
    addButton.textContent='aggiungimi';
    addButton.addEventListener('click', function () {
        addTodo(document.getElementById('addTodoInput').value);
    });
    divApp.appendChild(addButton);

    // Attccho l'evento click del bottone



    // Creo la lista in JS
    var ulList = document.createElement('ul');
    ulList.id="todosList";
    //Aggiungo la lista lla fine del container div#app
    divApp.appendChild(ulList);

    reloadTodo();


}






// Associo l'evento DOMControllerLoaded
// Alla funzione main
document.addEventListener("DOMContentLoaded", main);

