Vue.createApp({
    async mounted() {
        await this.reloadTodos();
        this.loaded = true;
    },
    template: `
      <h1>TODO APP</h1>
      <diV>
      <input id="inp" placeholder="Nuovo todo" type="text" v-model="newTodoDescription" />
      <button @click="addTodo()">Aggiungi</button>

      </div>
      <!-- <input id="newDesc" type="text" placeholder="Nuova descrizione" v-model="updateTodoDescription" /> -->
      <svg  v-if="!loaded" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style=" background: rgb(255, 255, 255); display: block; shape-rendering: auto; margin-top: 50px; margin-left: 70px;" width="60px" height="60px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <path d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#e15b64" stroke="none">
          <animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite" keyTimes="0;1" values="0 50 51;360 50 51"></animateTransform>
        </path>
      </svg>
      <div v-if="todos.length <= 0 && loaded">Non ci sono todos</div>
      <ul id="ul">
        <li v-for="todo in todos"><i @click="deleteTodos(todo.id)" class="mdi mdi-delete"></i><input @click="toggleTodo(todo)" type="checkbox" :checked="todo.done===1"> {{ todo.description }} </li>
      </ul>
    `,
    data() {
      return {
        newTodoDescription: '',
        todos: [],
        loaded: false,
          updateTodoDescription: '',
      }
    },
    methods: {
      async addTodo() {
        await fetch('http://localhost:8080/api/todos', {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({description: this.newTodoDescription})
        });
        this.reloadTodos();
      },
      async reloadTodos() {
        var todosJSON = await (await fetch('http://localhost:8080/api/todos')).json();
        this.todos = todosJSON;
        ul = document.getElementById("ul");
        ul.hidden = false;
        this.newTodoDescription = '';
        this.updateTodoDescription = '';

      },
      async deleteTodos(id) {
        await fetch('http://localhost:8080/api/todos/' + id, {
            method: 'DELETE',
            headers: {'Content-type' : 'application/json'}
        }, );
        this.reloadTodos();
      },
        done(checked) {
            if (checked === 0) {
                return false
            } else {
                return true;
            }
        },
        async toggleTodo(todo) {
            await fetch('http://localhost:8080/api/todos/' + todo.id, {
                method: 'PUT',
                headers: {'Content-type' : 'application/json'},
                body: JSON.stringify({
                    description: todo.description,
                    done: todo.done === 1 ? 0 : 1
                })
            });
            this.reloadTodos();
        }


    }
  }).mount('#app');
